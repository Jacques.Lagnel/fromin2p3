# distribution based on: debian 9.8
Bootstrap:docker
From:debian:9.8-slim

# container for smartdenovo v1.0.0
# Build:
# sudo singularity build smartdenovo_v1.0.0.sif Singularity.smartdenovo_v1.0.0

%environment
export LC_ALL=C
export LC_NUMERIC=en_GB.UTF-8
export PATH="/opt/miniconda/bin:$PATH"

#%labels
#COPYRIGHT INRAe GAFL 2020
#AUTHOR Jacques Lagnel
#VERSION 1.1
#LICENSE MIT
#DATE_MODIF MYDATEMODIF
#smartdenovo (v1.0.0)

%help
Container for smartdenovo
Ultra-fast de novo assembler using long noisy reads
https://github.com/ruanjue/smartdenovo

Version: 1.0.0
Package installation using Miniconda3 V4.7.12
All packages are in /opt/miniconda/bin & are in PATH
Default runscript: smartdenovo.pl

Usage:
    smartdenovo_v1.0.0.sif --help
    or:
    singularity exec smartdenovo_v1.0.0.sif smartdenovo.pl --help


%runscript
    #default runscript: smartdenovo.pl passing all arguments from cli: $@
    exec /opt/miniconda/bin/smartdenovo.pl "$@"

%post

    #essential stuff but minimal
    apt update
    #for security fixe:
    #apt upgrade -y
    apt install -y wget bzip2

    #install conda
    cd /opt
    rm -fr miniconda

    #miniconda3: get miniconda3 version 4.7.12
    wget https://repo.continuum.io/miniconda/Miniconda3-4.7.12-Linux-x86_64.sh -O miniconda.sh

    #install conda
    bash miniconda.sh -b -p /opt/miniconda
    export PATH="/opt/miniconda/bin:$PATH"
    #add channels
    conda config --add channels defaults
    conda config --add channels bioconda
    conda config --add channels conda-forge

    #install smartdenovo
    conda install -c bioconda smartdenovo=1.0.0

    #cleanup
    conda clean -y --all
    rm -f /opt/miniconda.sh
    apt autoremove --purge
    apt clean

